# AutoBitly
Python command line tool that converts all URLs in a plain text document to Bit.ly links using the Bit.ly API. Great for writing many different social media posts in a text file, then using this script to shorten all the URLs before posting.

> This script has been tested on Linux only.

## Getting Started

### Prerequisites

You will need:
* Python 3.9
* A [Bitly](https:bit.ly) Account

### Installing

Clone the repository from Gitlab

```
git clone https://gitlab.com/twoodwar/autobitly.git
```
#### Get Your Bit.ly API Key

Use the Bitly API to get a token using the [HTTP basic authentication flow](https://dev.bitly.com/docs/getting-started/authentication) and save it as a environment variable called `BITLY_TOKEN` in your terminal environment.

On Linux:
Use `curl` to retrieve your Bitly API token and save to a text file `token.txt`:
```
curl -u "username:password" -X POST "https://api-ssl.bitly.com/oauth/access_token" > token.text
```
Add the contents of the `token.txt` file to an environment variable (this will only last until the current shell is closed)
```
export BITLY_TOKEN=`cat token.txt`
```
Delete `token.txt` 
```
rm token.txt
```
> You can set the environment variable in your `.bashrc` so you do not need to retrieve your API Token every time you run the program, but this is insecure, do so at your own risk.
#### Install Dependencies

After cloning the repo, use `requirements.txt` to download the required packages
```
pip install -r requirements.txt
```

#### Usage

First argument should be the path to the file you want to modify, with an optional `-o` parameter if you would like the output file to have a particular name. If you leave the `-o` parameter off, it will default to using `autobitlyoutput.txt`. 

Example:
```
autobitly 2021-01-24-Social-Media-Posts.txt -o 2021-01-24-Social-Media-Posts-Shortened.txt
```

## Versioning

This repo uses [SemVer](http://semver.org/) for versioning. 

## Authors

* **Tyler Woodward** - [twoodwar](https://gitlab.com/twoodwar)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
