import bitly_api
import argparse
import os
import sys

if os.environ.get("BITLY_TOKEN") ==  None:
    sys.exit("Your BITLY_TOKEN environment variable is empty, please put your Bit.ly token in this environment variable.")

bitly_token = os.environ.get("BITLY_TOKEN")

parser = argparse.ArgumentParser(prog = "autobitly", description="Converts all URLs in a plain text document to Bit.ly links using the Bit.ly API.")
parser.add_argument("Path", help="Path to the text file you want all URLs in the file to change to Bit.ly links", metavar="path",type=str)
parser.add_argument("-o","--OutFile", help="Name of the output file", type=str, default="autobitlyoutput.txt")

args=parser.parse_args()

input_path = args.Path

if not os.path.isfile(input_path):
	sys.exit("The path specified does not lead to a file")

input_file = open(input_path, "r")
output_file = open(args.OutFile, "w")

bitly_connection = bitly_api.Connection(access_token=bitly_token)

for line in input_file:
	if "http" in line:
		#Find the substring 'http' in the current line. Treat all text between http and the next space character as the URL.
		url_start_position = line.find("http")
		url_end_position = line.find(" ",url_start_position,-1)		
		url = line[url_start_position:url_end_position]

		#Grab the text before and after the URL
		text_before_url = line[:url_start_position]
		text_after_url = line[url_end_position:-1]

		returned_info = bitly_connection.shorten(url)

		shortened_url= returned_info["url"]

		#Write everything to the output file
		output_file.write(text_before_url + shortened_url + text_after_url)
	else:
		output_file.write(line)

input_file.close()
output_file.close()
